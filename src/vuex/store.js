import Vue from 'vue';
import Vuex from 'vuex';
import Axios from 'axios';


Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        employees: null,
    },
    getters: {
        EMPLOYEES: state =>{
            return state.employees;
        },
    },
    mutations: {
        SET_EMPLOYEES: (state,payload) => {
            state.employees = payload;
        },


    },
    actions: {

        GET_EMPLOYEES: async (context) => {
            let {data} = await Axios.get('http://localhost:8000/api/v1/employees/?page=1');
            // .get(`http://localhost:8000/api/v1/employees/?page=${this.currentPage}`)
            context.commit('SET_EMPLOYEES', data.results);
        },


        GET_EMPLOYEES_NEW_PAGE: async (context, numberPage) => {
            let {data} = await Axios.get(`http://localhost:8000/api/v1/employees/?page=${numberPage}`);
            context.commit('SET_EMPLOYEES', data.results);
        },

        GET_EMPLOYEES_FILTER_AGE: async (context, age) => {
            let {data} = await Axios.get(`http://localhost:8000/api/v1/employees/?age=${age}`);
            context.commit('SET_EMPLOYEES', data.results);
        },

        GET_EMPLOYEES_SEARCH: async (context, searchValue) => {
            let {data} = await Axios.get(`http://localhost:8000/api/v1/employees/?search=${searchValue}`);
            context.commit('SET_EMPLOYEES', data.results);
        },



    },
});