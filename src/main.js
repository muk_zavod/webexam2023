import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import HelloWorld from "./components/HelloWorld";
import EmployeeComp from "@/components/EmployeeComp";
import EmployeesComp from "@/components/EmployeesComp";
import SignInComp from "@/components/SignInComp";
import RegisterComp from "@/components/RegisterComp";
import DepartmentsComp from "@/components/DepartmentsComp";

import QComp from "@/components/QComp";
import {store} from './vuex/store'
import { ValidationProvider } from 'vee-validate';

Vue.use(VueRouter);

const routes = [
  {
    path: "",
    name: 'main',
    component: HelloWorld,
  },


  {
    path: "/signin",
    component: SignInComp,
  },

  {
    path: "/employees/departments",
    component: DepartmentsComp,
  },

  {
    path: "/register",
    component: RegisterComp,
  },


  {
    path: "/employees",
    name: 'employees',
    component: EmployeesComp,
  },


  {
    path: "/employees/:id",
    name: 'NewId',
    component: EmployeeComp,
  },

  {
    path: "/employees/q",
    name: 'q',
    component: QComp,
  }
];



const router = new VueRouter({
  routes,
  mode: "history",
});

Vue.config.productionTip = false;

new Vue({
  ValidationProvider,
  store,
  router,
  render: (h) => h(App)
}).$mount("#app");


